# librft

AX.25 oriented file transfer protocol used to provide highly reliable ack-less 
file transfer between Linux systems connected by means of a UHF physical layer

## Documentation

Documentation can be generated using `doxygen Doxyfile` from the root directory of the project. The output can be viewed in a web browser by opening `doc/doxygen/html/index.html`

## Building

CMake is the primary build system. If you've never used it, the default CMake config is sane, and you can build the code using the following shell command (executed from the project root)

`mkdir build && cd build && cmake .. && make`

Afterwards, you can run the built-in test suite with

`make test`

Since this library is embedded, and therefore not useful to install on development machines, there is no `install` target. Instead, find the `.so` file in the same directory relative to build that `librft.c` is found relative to project root, eg `lib/librft.c -> build/lib/librft.so`

## Integrating into existing CMake

TODO

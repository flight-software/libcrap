# libcrap File Transfer Protocol

## Terminology
| Term | Definition |
|---|---|
| user | A *user* is either the client or the server in the protocol. |
| client | A *client* is the receiver of the file at the other end of the link. Their behavior is entirely passive, consuming data from their receive buffers and routing it to the appropriate file.|
| server | A *server* is the sender of the file. They do not negotiate any sort of connection with the client. |
| chunk | A *chunk* is a small piece of a file, complete with unique file routing information. For a given session, all chunks are the same size, and must fall within the MTU of the given radio.
| session | A *session* consists of a sequence of transmissions from the server to the client, through which the file is transferred.|
| half-duplex | A *half-duplex* physical layer allows sending and receiving over the same frequency, which prevents simultaneous transmission and receipt.|
| MTU | Maximum Transmission Unit, in bytes. This refers to the number of bytes available *to the protocol* in a single RF packet. For example, on the GOMSpace AX100, the MTU is only 221 bytes, since the radio uses two bytes for libcsp header information, and 32 bytes for Reed-Solomon parity. |
| chunk sequence | A chunk sequence is an uninterrupted sequence of chunks that occurs within a given session. Each session will contain at least one chunk sequence. |
| chunk sequence limit | The chunk sequence limit is the maximum number of chunks in a given chunk sequence before a new header is sent. |
| chunk sequence identifier | The chunk sequence identifier (abbreviated CSID) is a unique number applied to a given chunk sequence, and will be present on all chunks in the chunk sequence, so as to prevent chunks from other files/sessions from corrupting the current file/session. It changes every time the file transfer header is recomputed. |
| lost chunk | A chunk identified by the protocol as being missing due to packet loss. |
| lost chunk request count | The number of times that the server will repeat a lost chunk request. |

## Protocol visualization
```
CLIENT                                          SERVER
   | <--------------- file transfer header --------|
   | <--------------- chunk sequence --------------|
                    (repeat till EOF)
   | <--------------- lost chunk requests ---------|
   | ---------------- lost chunks desired -------->|
   | <--------------- file transfer header --------|
   | <--------------- lost chunk sequence ---------|
            (repeat until no more lost chunks)
```

## Protocol visualization (resuming uplink)
```
CLIENT                                          SERVER
   |<---------------- file transfer header --------|
        (repeat lost chunk request count times)
   |<---------------- lost chunk requests ---------|
   |----------------- lost chunks desired -------->|
   |<---------------- file transfer header --------|
   |<---------------- lost chunk sequence ---------|
            (repeat until no more lost chunks)
```

## Protocol specification
In order to achieve ack-less transmission, it is assumed that the client will
never give an indication that it has successfully received individual chunks,
as the timeouts involved to reliably get these acknowledgements significantly
affect the reliability of the protocol over a half-duplex link.

The most important part is to remove as much back-and-forth communication as possible,
so that the full advantages of burst transmission can be relied upon.

In removing these acknowledgements, we craft a session as such:

Server side: 

1. To start the session, provide libcrap on the server with the path to a file
that you would like to transmit, as well as callback functions to send and
receive buffers.
2. The server will first send the header containing file routing information,
followed by a number of chunks equal to the maximum of either the chunk sequence
limit, or the number of chunks remaining in the file.
3. Once the server reaches the chunk sequence limit, it will recompute the header
and start back at step 2. This repeats until all chunks are transmitted.
4. After all chunks are transmitted, the server will transmit a fixed number of
"lost chunk" requests, then it will switch into a receive mode. This number, 
called the lost chunk request count, can be customized for a given session.
If the server does not send these requests, the client will time out and consider
the session complete.
5. The client will respond with the same number of lost chunk packets, where each
packet requests the exact same chunks, for redundancy. If the client is silent,
the protocol concludes.
6. The server will seek to retransmit the chunks requested, and proceed to step 4.

Client side:

1. Clients will always passively listen. They retain a copy of the latest file
transfer header they have received from the server, and automatically place the
chunks received into this file. Packet timeout is specified when the client
starts listening.
2. When receiving chunks, they check to see that the CSID received with the 
latest header is still valid (eg has not timed out), and if not, they discard 
all packets that are not file transfer headers. The invalid packet callback is
called on these packets, if applicable.
3. File transfer headers are checked for protocol version compatibility, and will
be accepted only if the version is greater than or equal to the version running
on the client.
4. Chunks are saved locally so the file may be reconstructed even if there is a
loss of power during the transfer.
5. If lost chunk requests are received, the client calculates which chunks are
required to be retransmitted (based on the most recent file transfer header,
and the local chunks it has saved). It will not transmit the constructed lost chunk
packet until the lost chunk requests have all come in, or in the case of packet loss,
the amount of time requred for these requests has elapsed (see "Calculating when 
to reply with lost chunks desired").
6. The lost chunks desired are transmitted the same number of times as the number
of lost chunk requests. This is not the same as the number of lost chunk requests
received, as that may be less the overall number transmitted by the server.
7. If at any point the packet timeout expires, and no packet has been received,
the client will consider the session complete.
8. At the conclusion of the session, the client will attempt to assemble the
chunks it has using the file transfer header it received most recently.

## Calculating when to reply with lost chunks desired

Because the number of lost chunk requests transmitted by the server is included
in every lost chunk request, as well as the ID of the current request, packet
loss of individual requests can be tolerated, while preserving the ability to
predict when the lost chunk requests will conclude without having to wait for
costly timeouts.

Consider the situation where the lost chunk request count is 30, and we receive
the following lost chunk requests on the client:

|lost chunk request ID|time of receipt (relative to last file chunk, ms)|
|---|---|
|0|t0|
|1|t1|
|12|t2|
|21|t3|
|25|t4|

On a timeline, this looks like:

```
ID   |      0     1           12             21      25
Time |      t0    t1          t2             t3      t4
```

This implies we can transmit:

1 packet in (t1-t0) ms
11 packets in (t2-t1) ms
9 packets in (t3-t2) ms
4 packets in (t4-t3) ms

So the best guess for how long it takes per packet is the average of these
average times, eg:

```
dt/packet = mean(t1-t0, 1/11 (t2-t1), 1/9 (t3-t2), 1/4 (t4-t3))
```
This average can be calculated in a rolling fashion, and used to adjust the timeout
window for when the client begins transmitting the reciprocal lost chunks desired.

In the case of a single lost chunk request received at time t0 after the last chunk,
the average time is simply t0/(request ID + 1), and the time the client needs to wait
can be calculated from that.

## Receive Callback
Users will be expected to provide a callback function by which the client or server
can receive data. The function signature will be like so:

`int receiver(uint8_t *destination_buffer, size_t *bytes_received, struct timeval *timeout)`

**Requirements**
1. Bytes received will be expected in `destination buffer`
2. The number of bytes the protocol expects to receive will be pointed to by `bytes_received`
3. The number of bytes actually received must be placed in `*bytes_received`
4. A nonzero error code must be returned to indicate failure, including timeout, hardware error, etc
5. A return code of `EXIT_SUCCESS` will be interpreted as success
6. A return code of `R_TIMEOUT` must be returned to indicate timeout
7. The function must timeout after the amount of time in `timeout`, see `man select`
8. The callback may modify the timeout struct

The callback must be initially registered using `crap_init(...)`, but may be re-registered
during the protocol using `crap_register_recv_cbk(crap_recv_buffer_t)`.

## Transmit Callback
Users will be expected to provide a callback function by which the client or server
can transmit data. The function signature will be like so:

`int transmitter(const uint8_t *transmit_buffer, size_t bytes_to_send, struct timeval *timeout)`

**Requirements**
1. Bytes to send will be found in `transmit_buffer`
2. The number of bytes the protocol expects to send will be in `bytes_to_send`
3. A nonzero error code must be returned to indicate failure, including timeout, less bytes sent than expected, etc
4. A return code of `EXIT_SUCCESS` will be interpreted as success.
5. A return code of `R_TIMEOUT` must be returned to indicate timeout
6. The function must timeout after the amount of time in `timeout`, see `man select`
7. The callback may modify the timeout struct

## Parity
It is the belief of the developer that parity should be handled at the level
of the transmit/receive callbacks, as every radio configuraiton will have
a different way to handle parity, some may require parity poisoning, etc.

This keeps the library simple and unaware of the error correction needed,
and allows it to directly inspect, eg, synchronization bytes.

In code, this may be thought of like so:
```
int receive_buffer_wrapper(uint8_t *buffer, size_t *length, struct timeval *timeout) 
{
    int err = 0;
    // the hardware may read more bytes than length due to added
    // parity; this is up to the users to decide.
    err = receive_buffer_from_hardware(buffer, length, timeout);
    if(err) {
        return err;
    }
    // This function will also correct errors if found
    err = apply_parity_check(buffer, length);
    return err;
}

int transmit_buffer_wrapper(const uint8_t *buffer, const size_t length, struct timeval *timeout)
{
    int err = 0;
    size_t new_length = length;
    uint8_t *new_buffer = new_buffer_with_room_for_parity(buffer, &new_length);
    err = apply_parity_to_buffer(new_buffer, new_length);
    if(err) {
        free(new_buffer);
        return err;
    }
    err = transmit_buffer_to_hardware(new_buffer, new_length, timeout);
    free(new_buffer);
    return err;
}
```

These are merely suggestions at how it may be implemented, and should be designed
with the hardware in mind.

## Invalid packet callback
The user of the library will be able to specify a callback function which is called
when a packet is received that is not classified as a valid protocol packet. This
may be used to route out of order packets to other protocols. 

## Protocol header
```
fixed synchronization bytes (uint8_t)
protocol version (BE uint16_t)
```

## File transfer header
```
protocol header
File path // Expected to be exactly 128 bytes, extra chars must be 0x0
File mode // mode_t
File size (bytes) // little endian
chunk sequence identifier (CSID) // little endian
CSID validity time (in seconds) // little endian
protocol MTU (number of chunks = ceiling(file size / protocol mtu))
```

## File chunks
```
protocol header
CSID // little endian
chunk identifier // little endian
raw file data (to be placed into the file)
```

## Lost chunks desired packet

```
protocol header
uint8_t request_id;
uint8_t request_total;
uint32_t chunk_ids[MTU//sizeof(*chunk_ids)]; // TODO: not actually, MTU, but whatever remains
```

## Saving chunks and file transfer headers

when a file transfer header is received, a directory is created under `$LIBCRAP_PARTIAL_PREFIX`.
If this environment variable does not exist, the default path is used: `~/.cache/libcrap/partial`
The directory will have the same name as `dirname(file_path)`, where `file_path` is the
path of the file specified in the file transfer header. The header will be saved to

```
$LIBCRAP_PARTIAL_PREFIX/dirname(file_path)/basename(file_path).fitfheader
```

The chunks received will be saved to

```
$LIBCRAP_PARTIAL_PREFIX/dirname(file_path)/basename(file_path).chunk_id
```

## File destinations
When a file is completely received, it is assembled and placed in an uplink directory
for the users to move elsewhere. Once completed, all saved chunks and file transfer
headers for the file are removed.

Files received by the client will be placed into a virtual root filesystem that
is prefixed by the directory defined in `$LIBCRAP_PREFIX`. If this environment
variable is not defined on the client, or is not a valid directory, files will
be placed in the directory `~/.cache/libcrap/uplink`.

As an example, a file whose full path on the server is `/home/user/myfile.txt`
will be received on the client at `~/.cache/libcrap/uplink/home/user/myfile.txt`

## Broken stuff
Currently there's no way for the client to announce if it has a smaller MTU than the server.
The fix is for the client to just be silent? TODO

/**
 * @file librft.c
 * librft library implementation. Documentation found in header file
 */
#include "librft.h"

static rft_invalid_callback_t invalid_packet_cbk = NULL;
static rft_recv_buffer_t rft_recv = NULL;
static rft_send_buffer_t rft_send = NULL;

/**
 * @file librft.h
 * librft library header file, containing public and private APIs and all macros
 */
#pragma once

#include <stdio.h> /* printf, fprintf, vsnprintf, etc */
#include <stdlib.h> /* secure_getenv */
#include <stdint.h> /* uint8_t uint32_t, etc */
#include <stddef.h> /* size_t */
#include <endian.h> /* be16toh, etc */
#include <sys/stat.h> /* mode_t, stat, fstat, access, mkdir */
#include <errno.h> /* errno */
#include <string.h> /* mem*, strerror */
#include <sys/time.h> /* struct timeval */

/** 
 * @def RFT_MAX_PATH_LEN
 * @brief Maximum length of a path allowed in the protocol 
 */
#define RFT_MAX_PATH_LEN 128
/**
 * @def RFT_MAX_MTU
 * @brief Maximum MTU supported by the protocol
 * @todo make settable at compile time
 */
#define RFT_MAX_MTU 255
/**
 * @def RFT_CSID_VALIDITY
 * @brief Amount of time, in seconds, that a chunk sequence identifier is valid
 */
#define RFT_CSID_VALIDITY 60
/**
 * @def RFT_INVALID_CHUNK_ID
 * @brief default chunk ID, used to distinguish good chunk IDs from bad chunk IDs
 */
#define RFT_INVALID_CHUNK_ID    UINT32_MAX
/**
 * @def RFT_INVALID_TIMEVAL
 * @brief default timeval struct, used to distinguish good timevals from bad ones
 */
#define RFT_INVALID_TIMEVAL {               \
    .tv_sec = 0,                            \
    .tv_usec = 0                            \
}
/**
 * @def RFT_INVALID_VERSION
 * @brief default protocol version, used to distinguish good versions from bad ones
 */
#define RFT_INVALID_VERSION     (UINT16_C(0))
/**
 * @def RFT_INVALID_SYNC
 * @brief default protocol sync, used to distinguish valid protocol syncs from invalid ones
 */
#define RFT_INVALID_SYNC        (UINT8_C(0))
/**
 * @def RFT_INVALID_HEADER
 * @brief default protocol header, used to distinguish good headers from bad ones
 */
#define RFT_INVALID_HEADER {            \
    .tag = {                            \
        0,0,0                           \
    }                                   \
    .version = RFT_INVALID_VERSION,     \
    .sync = RFT_INVALID_SYNC            \
}
/**
 * @def RFT_INVALID_PACKET
 * @brief default packet, used to distinguish good packets from bad ones
 */
#define RFT_INVALID_PACKET {            \
    .header = RFT_INVALID_HEADER,       \
    .data = {                           \
        0                               \
    }                                   \
}
/**
 * @def RFT_INVALID_TIMED_PACKET
 * @brief default timed packet, used to distinguish good timed packets from bad ones
 */
#define RFT_INVALID_TIMED_PACKET {              \
    .time_received = RFT_INVALID_TIMEVAL,       \
    .packet = RFT_INVALID_PACKET                \
}
/**
 * @def RFT_PREFIX_ENV
 * @brief Environment variable used to define where uplinked files are stored 
 */
#define RFT_PREFIX_ENV "LIBRFT_PREFIX" 
/**
 * @def RFT_PARTIAL_PREFIX_ENV
 * @brief Environment variable used to define where partially uplinked files are stored */
#define RFT_PARTIAL_PREFIX_ENV "LIBRFT_PARTIAL_PREFIX" 
/**
 * @def RFT_BASE_PATH
 * @brief default path from which librft operates 
 *
 * This parameter can be configured at compile time in the CMake using the "RFT_BASE_PATH" option
 */
#ifndef RFT_BASE_PATH
#define RFT_BASE_PATH ("~/.cache/librft") 
#endif /* RFT_BASE_PATH */
/**
 * @def RFT_PREFIX
 * @brief path relative to base path where uplinked files are placed 
 */
#define RFT_PREFIX ("uplink") 
/** 
 * @def RFT_PARTIAL_PREFIX
 * @brief path relative to base path where partially uplinked files are placed 
 */
#define RFT_PARTIAL_PREFIX ("partial") 

/**
 * @page sync 
 * @section sync_sec1 Synchronization Bytes
 *
 * Synchronization bytes are used to identify data that may come from an unreliable source is formatted in the correct format. Typically they're prepended to an incoming packet and will be checked before anything potentially harmful is done with the packet.
 *
 * Synchronization bytes are derived from Hamming-distance 5
 * lexical encodings, table found here: http://burtleburtle.net/bob/math/lexi5.txt
 * 
 * Remaining unused sync bytes: 0xf7, 0xed, 0xdb, 0xab, 0x96 
 */
/**
 * @page csid
 * @section csid_sec1 Chunk Sequence Identifier
 *
 * The <i>chunk sequence identifier</i> is a unique 2-byte number sent alongside the file chunks
 * in a chunk sequence to ensure they have a lifetime, and are not written to a file they are 
 * not intended for. It expires within the CSID validity time. 
 */
/** 
 * @def RFT_SYNC_FITF
 * @brief Synchronization byte attached to file transfer headers 
 * @see @ref sync Synchronization Bytes
 */
#define RFT_SYNC_FITF   (UINT8_C(0x0f))
/**
 * @def RFT_SYNC_CHUNK
 * @brief Synchronization byte attached to file chunks 
 * @see @ref sync Synchronization Bytes
 */
#define RFT_SYNC_CHUNK  (UINT8_C(0x33))
/** 
 * @def RFT_SYNC_LOST_REQ
 * @brief Synchronization byte attached to lost chunk requests 
 * @see @ref sync Synchronization Bytes
 */
#define RFT_SYNC_LOST_REQ       (UINT8_C(0x55))
/** 
 * @def RFT_SYNC_LOST_DES
 * @brief Synchronization byte attached to lost chunks desired 
 * @see @ref sync Synchronization Bytes
 */
#define RFT_SYNC_LOST_DES       (UINT8_C(0x6a))

/**/

/**
 * Error codes emitted by the protocol
 */
/**
 * @def R_ERR(n)
 * @brief Helper macro used to generate error codes unsued by glibc
 * @param n Offset after last error used for this error code
 */
#define R_ERR(n)        (__ELASTERROR + (n))
/**
 * @def R_MTU_TOO_SMALL
 * @brief MTU allowed by the protocol does not allow protocol to work correctly 
 */
#define R_MTU_TOO_SMALL         R_ERR(1)
/**
 * @def R_CSID_EXPIRED
 * @brief chunk sequence identifier too old 
 */
#define R_CSID_EXPIRED          R_ERR(2)
/**
 * @def R_INVALID_PACKET
 * @brief packet received is not a protocol packet 
 */
#define R_INVALID_PACKET        R_ERR(3)
/** 
 * @def R_TIMEOUT
 * @brief Timeout exceeded for new packet to come in 
 */
#define R_TIMEOUT               R_ERR(4)
/**
 * @def R_SESSION_COMPLETE
 * @brief session finished, no errors to report 
 */
#define R_SESSION_COMPLETE      R_ERR(5)
/**
 * @def R_INCOMPLETE_SESSION
 * @brief Assumptions broken about current session, eg unregistered callbacks, mtu too large 
 */
#define R_INCOMPLETE_SESSION    R_ERR(6)
/**
 * @def R_PATH_TOO_LONG 
 * @brief Path of file that server is attempting to transmit is too long
 * @see RFT_MAX_PATH_LEN
 */
#define R_PATH_TOO_LONG         R_ERR(7)
/**
 * @def R_MTU_TOO_BIG
 * @brief MTU specifed is too large for protocol
 */
#define R_MTU_TOO_BIG           R_ERR(8)
/**
 * @def R_UNINITIALIZED
 * @brief An indicator that one or many session parameters was left in an uninitialized state
 */
#define R_UNINITIALIZED         R_ERR(9)
/**
 * @def R_IO
 * @brief Indicative of IO problems during the protocol, eg with fwrite
 */
#define R_IO                    R_ERR(10)
/**
 * @def R_UNEXPECTED_PACKET
 * @brief Packet received was not expected at this part of the protocol
 */
#define R_UNEXPECTED_PACKET     R_ERR(11)

/**
 * @brief Protocol header, expected at the beginning of every packet 
 *
 * To identify packets that are meant for the protocol from packets that
 * are destined for other places, a header is applied to all valid
 * protocol packets. This header contains synchronization bytes
 * for packet types, as well as a version identifier used to allow future
 * expansion of the protocol without breaking compatibility
 */
typedef struct __attribute__((packed)) rft_header_t {
    char tag[3]; /**< will contain the string 'RFT' */
    uint16_t version; /**< version of protocol used */
    uint8_t sync; /**< synchronization byte, used to identify packet type */
} rft_header_t;

/**
 * @brief Initialize a protocol header
 * @param header Pointer to header that needs initialization
 * @param sync Synchronization byte indicating packet type
 */
int _rft_init_header(rft_header_t *header, uint8_t sync);

/** 
 * @brief File transfer header, all info necessary to reconstruct the file 
 */
typedef struct __attribute__((packed)) rft_fitf_t {
    rft_header_t header; /**< protocol header 
                          * @see rft_header_t
                          */
    char file_path[RFT_MAX_PATH_LEN]; /**< path of file being uplinked on the server */
    mode_t file_mode; /**< @see <sys/stat.h> */
    uint32_t file_size; /**< little endian file size, in bytes, of the file to be transmitted. Maxes out at 4GiB, which is more than reasonable for UHF */
    uint16_t csid; /**< chunk sequence identifier */
    uint16_t csid_validity_time; /**< number of seconds after first csid receipt that the csid is valid */ 
    size_t mtu; /**< number of bytes server allows the protocol to use in the packet */
} rft_fitf_t;

/**
 * @brief Protocol packet with header data accessible
 */
typedef struct __attribute__((packed)) rft_packet_t {
    rft_header_t header; /**< protocol header */
    uint8_t data[RFT_MAX_MTU-sizeof(rft_header_t)]; /**< remaining data in the packet */
} rft_packet_t;

/**
 * @brief Packet tied to time of receipt. Used to track the end of bursts
 *
 * When a packet is received and it is identified as a protocol packet in the lost chunks
 * phase of the protocol, it is tagged with a time in order to predict the end of the incoming
 * burst of requests/desired packets. By predicting the end and ending at the predicted time,
 * there is no need to wait for a timeout
 */
typedef struct __attribute__((packed)) rft_timed_packet_t {
    struct timeval time_received; /**< Time when this packet was received */
    rft_packet_t packet; /**< Packet data, may be freely cast to a known packet type */
} rft_timed_packet_t;

/**
 * @brief Create timed packet from received packet
 * @param timed_packet
 * @param packet
 * @return EXIT_SUCCESS if everything is okay
 * @return EXIT_FAILURE if the pointers are NULL
 * @return R_INVALID_PACKET if the incoming packet is not a protocol packet
 * @see rft_timed_packet_t
 */
int _rft_init_timed_packet(rft_timed_packet_t *timed_packet, rft_packet_t *packet);

/**
 * @brief Keeps track of packet with smallest request_id and largest request_id, using them to
 * calculate when the burst will end
 *
 * @todo get_request_id
 * @todo get_request_total
 */
typedef struct __attribute__((packed)) rft_burst_predictor_t {
    rft_timed_packet_t min_packet; /**< packet with the smallest request_id */
    rft_timed_packet_t max_packet; /**< packet with the largest request_id */
    int (*get_request_id)(rft_packet_t *, uint32_t *); /**< function which gets the request ID from a packet */
    uint32_t (*get_request_total)(rft_packet_t); /**< function which gets the total number of packets in the burst from a packet */
    int (*burst_is_over)(void*); /**< function which looks at current time and known packets, and indicates whether or not burst is over */
} rft_burst_predictor_t;

/**
 * @brief Initialize a burst predictor object
 * @param predictor Burst prediciton object to initialize
 */
int _rft_init_burst_predictor(rft_burst_predictor_t *predictor);

/**
 * @brief inspect the burst predictor packet times and guess when the burst will end
 * @param predictor Burst predictor instance
 * @return 1 if burst is over
 * @return 0 if burst is ongoing
 */
int _rft_burst_is_over(void *predictor);

/** 
 * @brief Lost chunks request, repeated lost_chunk_req_count times by server
 * @see rft_sess_t
 *
 * Once the initial pass over all chunks has been completed by the server,
 * the server will send a repeated sequence of lost chunk requests to the
 * client, to which the client will respond with the chunks it desires as the
 * only ack-ed part of the protocol
 *
 * The request is the same as the header information in the rft_lost_chunk_des_t
 */
typedef struct __attribute__((packed)) rft_lost_chunk_header_t {
    rft_header_t header; /**< protocol header
                          * @see rft_header_t
                          */
    uint32_t request_id; /**< little endian identifier of where in the sequence of lost chunk requests this request lies */
    uint32_t request_total; /**< little endian, total number of requests issued by user */
} rft_lost_chunk_header_t;

/**
 * @brief Lost chunks desired, repeated lost_chunk_req_count times by client
 * @see rft_sess_t
 *
 * When the client receives a request for all of the chunks it's missing, it
 * will respond with a subset of these chunks, as many as is allowed by the mtu.
 * This allows us to rely on the reliability of burst transmissions to ensure
 * the information gets back to the server
 */
typedef struct __attribute__((packed)) rft_lost_chunk_des_t {
    rft_lost_chunk_header_t header; /**< All info that is not the actual chunks desired */
    uint32_t chunks_desired[(RFT_MAX_MTU-sizeof(rft_lost_chunk_header_t))/sizeof(uint32_t)]; /**< pointer to buffer which contains all of the little endian chunk ids we want @see rft_chunk_t */
} rft_lost_chunk_des_t;

/**
 * @brief Retrieve the request ID from a protocol packet
 * @param packet Protocol packet which contains request ID
 * @param request_id 
 * @return R_INVALID_PACKET if the packet is not formatted to contain a request ID
 * @return EXIT_FAILURE if any pointers are NULL
 * @return EXIT_SUCCESS if the operation was successful
 */
int _rft_get_request_id(rft_packet_t *packet, uint32_t *request_id);

/**
 * @brief File chunk header, everything but the data
 */
typedef struct __attribute__((packed)) rft_chunk_header_t {
    rft_header_t header; /**<protocol header */
    uint16_t csid; /**< @ref csid CSID, little endian */ // TODO: CSID page
    uint32_t id; /**< chunk identifier, little endian */
} rft_chunk_header_t;

/**
 * @brief File chunk, which holds actual file data to send to client
 * 
 * We split the chunk into header and data so that we can have the data section
 * fill as much as possible. In practice, only as much as the currently allocated MTU
 * will be used.
 */
typedef struct __attribute__((packed)) rft_chunk_t {
    rft_chunk_header_t header; /**< @see rft_chunk_header_t */
    uint8_t data[RFT_MAX_MTU-sizeof(rft_chunk_header_t)]; /**<raw file data (to be placed into the file) */
} rft_chunk_t;

/**
 * @struct rft_sess_t
 * @brief Data structure used to contain all information relevant to current session 
 *
 * The session is an object used to manage all information pertaining to managing file
 * transfers in the protocol. The session keeps track of all callback functions, user settings,
 * hardware limitations, and other various diagnostics like the last packet receipt time.
 */
typedef struct __attribute__((packed)) rft_sess_t {
    rft_timed_packet_t last_packet; /**< Packet and time when we last received a packet */
    rft_fitf_t active_file; /**< a copy of the most recently received file transfer header */
    struct timeval csid_expiration; /**< time at which the current chunk sequence will no longer be accepted */
    size_t mtu; /**< maximum number of bytes usable by the protocol, taken as the minimum of the number of bytes server allows and the the number of bytes the client allows */
    size_t chunk_seq_lim; /**< maximum number of chunks allowed in a chunk sequence */
    size_t lost_chunk_req_count; /**< number of lost chunk requests made by the server, not necessarily the same as the number received by the client */ 
    struct timeval timeout; /**< default timeout used to determine if packets have timed out */
} rft_sess_t;

//typedef enum rft_state_t {
//    ChunkReceipt,
//    
//} rft_state_t;

/* Callback function type for invalid packets in the protocol */
typedef int (*rft_invalid_callback_t)(uint8_t *, size_t); 
/* Callback function currently registered for invalid packets */
static rft_invalid_callback_t invalid_packet_cbk; // TODO: put this into the session for reentrancy

/* Users must provide a way to receive a buffer from the hardware. See: Receiver Callback in specification */
typedef int (*rft_recv_buffer_t)(uint8_t *, size_t *, struct timeval *);
/* Callback function currently registered for receiving buffers */
static rft_recv_buffer_t rft_recv; // TODO: put this into the session

/* Users must provide a way to send a buffer to the hardware. See: Transmit Callback in specification */
typedef int (*rft_send_buffer_t)(const uint8_t *, size_t, struct timeval *);
/* Callback function currently registered for sending buffers */
static rft_send_buffer_t rft_send; // TODO: put this into the session

/**
 * Internal API
 */
/* File helpers */
// WIP: get size in bytes, get mode, create directories recursively
/**
 * @brief Retrieve the number of bytes in a file, safely cast to a u32
 * @param file_path Path to the file that needs to be sized. Shell expansion is performed, and symlinks are followed.
 * @param file_size Pointer to size variable into which the file size will be placed
 * @return Error status, EXIT_SUCCESS if successful
 */
int _rft_file_size(const char *file_path, uint32_t *file_size);
/**
 * @brief Retrieve the file mode of a file, as a mode_t
 * @param file_path Path to the file that needs to be sized. Shell expansion is performed, and symlinks are followed.
 * @param file_mode Pointer to mode variable into which the file mode will be
 * @return Error status, EXIT_SUCCESS if successful
 */
int _rft_file_mode(const char *file_path, mode_t *file_mode);
/**
 * @brief Recursively make parent directories of a provided file path
 * @param file_path Path to the file for which parent directories are desired
 * @return Error status, EXIT_SUCCESS if successful
 * @see www.opensource.apple.com/source/file_cmds/file_cmds-45/mkdir/mkdir.c
 */
int _rft_mkdir(const char *file_path);
/**
 * @brief NUL-terminate a provided string
 * @param string String to NUL-terminate
 * @param length Maximum length of string; NUL-term at this index
 * @return EXIT_SUCCESS
 * @todo make this check the strnlen to return better error codes
 */
int _rft_nulterm(char *string, size_t length);
/**
???END
 * @brief Set the chunk sequence ID validity time for a given session
 * @param sess Session on which to set the CSID validity time
 * @param validity Length of time, in seconds, that any CSID in the session is valid
 * @return Error status, EXIT_SUCCESS if successful
 * @see @ref advanced_api Advanced API // TODO: create page for advanced API
 */
int rft_set_csid_validity_time(rft_sess_t *sess, size_t validity);
/**
 * @brief Clear the contents of a file transfer header, setting defaults where applicable
 * @param fitf File transfer header to clear
 * @return Error status, EXIT_SUCCESS if successful
 */
int _rft_clear_fitf(rft_fitf_t *fitf);
/**
 * @brief Clear the csid_expiration timeval to a sentinel value indicated it was not initialized
 * @param sess Session whose csid_expiration field to clear
 */
int _rft_clear_csid_expiration(rft_sess_t *sess);
/**
 * @brief Set the session MTU, the number of bytes usable by the protocol
 * @param sess Session to set the MTU for
 * @param mtu MTU for the session
 * @see @ref advanced_api Advanced API
 *
 * This function may also be used during the session to change the MTU, which will
 * be reflected in the next chunk sequence 
 */
int rft_set_mtu(rft_sess_t *sess, size_t mtu);
/**
 * @brief Set the chunk sequence limit for the session
 * @param sess Session to use updated chunk sequence limit
 * @param chunk_seq_lim Maximum number of chunks to transfer in a given chunk sequence
 * @see doc/specification.md
 * @see @ref advanced_api Advanced API
 */
int rft_set_chunk_seq_lim(rft_sess_t *sess, size_t chunk_seq_lim);
/**
 * @brief Set the number of lost chunk requests to make
 * @param sess Session to set the number of lost chunk requests
 * @param lost_chunk_req_count Number of times to request lost chunks
 * @see doc/specification.md
 * @see @ref advanced_api Advanced API
 */
int rft_set_lost_chunk_req_count(rft_sess_t *sess, size_t lost_chunk_req_count);
/**
 * @brief Process a file chunk packet when we receive it
 *
 * Once identified as a file chunk packet, the packet must be routed to this function to
 * save its contents per doc/specification.md
 *
 * @return R_INVALID_PACKET if the sync does not match
 * @return R_IO if there was a problem writing the file chunk to disk
 * @return EXIT_SUCCESS if everything is normal
 */
int rft_process_chunk(rft_sess_t *sess, uint8_t *packet);
/**
 * @brief Process a lost chunk request when we receive it
 *
 * Once identified as a lost chunk request, the session information will be updated
 * per doc/specification.md
 *
 * @return R_INVALID_PACKET if the sync does not match
 * @return R_UNEXPECTED if we receive this as a server
 * @return EXIT_SUCCESS if everything is normal
 */
/* File transfer header helpers */
// update the csid, check the csid validity
// Both:
//      session creation
//              WIP: make sure file path is NUL-terminated
//              WIP: set csid validity time to default (TODO: allow user to modify)
//              WIP: clear active file
//              WIP: clear csid_expiration
//              WIP: set mtu based on provided mtu
//              WIP: set chunk seq lim to default (TODO: allow user to modify)
//              WIP: set lost chunk req count to default (TODO: allow user to modify)
//              WIP: set timeout based on provided timeout (TODO: place into rft_init directly)
//      lost chunk request prediction -> TODO: new type
//              rolling average
//              update from last packet time in session
//              when prediction is less than current time, return some kind of error indicating we should move on
//      session aware packet receiving
//              when we receive a packet, update the session
//              route the packet, then call the endian conversion function, then call the callback for processing the packet
// Server:
//      create file transfer header from session
//              copy file path
//              look up file mode
//              look up size
//      chunk iterator -> TODO: new type
//              init chunk iterator (from lost chunk request, or NULL to start at chunk id 0)
//              keep track of which chunk we sent last
//              get next chunk
//                      stop when we've reached the last chunk, or we have no more chunks
//                      we have no more chunks when the chunk ID is RFT_INVALID_CHUNK_ID
//              populate rft_chunk_t from session
//              send rft_chunk_t
//      lost chunk requests generator
//              get lost chunk request count from session
//              in for loop, initialize lost chunk requests, and send them
//      lost chunks desired read in
//              predict end of lost chunks desired burst
//              if burst is over, send lost chunks desired to chunk iterator
//              initialize chunks
//              transmit chunks
// Client:
//      receive fitf header
//          update session
//          create file directories from session information -> partial and uplink
//      receive file chunk
//          save chunk to partial directory, using naming scheme from specification
//      receive lost chunk request
//          predict end of lost chunk request burst
//          if burst is over, calculate desired chunks
//          send desired chunk burst
//
//
/**
 * 1. To start the session, provide librft on the server with the path to a file
that you would like to transmit, as well as callback functions to send and
receive buffers.
 * 2. The server will first send the header containing file routing information,
 * followed by a number of chunks equal to the maximum of either the chunk sequence
 * limit, or the number of chunks remaining in the file.
 * 3. Once the server reaches the chunk sequence limit, it will recompute the header
 * and start back at step 2. This repeats until all chunks are transmitted.
 * 4. After all chunks are transmitted, the server will transmit a fixed number of
 * "lost chunk" requests, then it will switch into a receive mode. This number, 
 * called the lost chunk request count, can be customized for a given session.
 * If the server does not send these requests, the client will time out and consider
 * the session complete.
 * 5. The client will respond with the same number of lost chunk packets, where each
 * packet requests the exact same chunks, for redundancy. If the client is silent,
 * the protocol concludes.
 * 6. The server will seek to retransmit the chunks requested, and proceed to step 4.
 */
/**
 * External API (simple)
 */
/**
 * Initialize an RFT session with which a session can be started
 * @param sess RFT session in which session information is stored
 * @param mtu maximum number of bytes usable for protocol by the user's radio 
 * @param recv_cbk Callback function used by the protocol to receive buffers
 * @param send_cbk Callback function used by the protocol to send buffers
 */
int rft_init(rft_sess_t *sess
        , size_t mtu 
        , rft_recv_buffer_t recv_cbk /* callback used to receive data */
        , rft_send_buffer_t send_cbk /* callback used to send data */
        , const struct timeval timeout /* packet timeout, used for recv and send */
    );
/**
 * @brief Print all session information to the FILE specified
 * @param sess Session whose information you'd like to save/view
 * @param stream Stream to which you would like to print
 */
int rft_print_session(rft_sess_t *sess, FILE *stream);
/**
 * External API (advanced)
 */
// register send, register recv, register invalid packet
// change the lost chunk request count
// change the chunk sequence limit

1) Opening curly brace on the same line, unless it's for a function  
2) Spaces not tabs, 4 spaces to a tab  
3) `snake_case`, eg:  
`my_variable_name`

not 

`myVariableName`

4) Pointer `*`'s are attached to variable names, eg:  
`int *p` not `int * p`  
5) Doxygen style comments  
6) When continuing paren onto another line, it must be same indent as leftmost token to which the paren is relevant  
7) No strings in CMake unless absolutely necessary  
8) After closing parens, one space is needed, eg:  
```
if(1) {
```

not 

```
if(1){
```
9) Parentheses are attached to the token they're relevant to, with no whitespace, eg:  
```
if(1) {
    ;
}
```

not

```
if (1) {
    ;
}
```
